import sys

def getFileNameFromPBXstring(pbxstring):
    # Get the filename, by pulling out the last multiline comment
    filename_helper = pbxstring.split("/*")[-1]
    return filename_helper.split("*/")[0].strip()

def findOneFromListInFileName(filename, listofpatterns):
    for i in listofpatterns:
        if i in filename:
            return True
    return False


def deconstructXcodeProject(xcodeproj="/Users/Donal/Documents/Projects/SAMPLETGAME/SAMPLETGAME.xcodeproj"):
    # I'm interested in project.pbxproj
    project_file_name = xcodeproj+"/project.pbxproj"
    # Opens the project file
    project_file = open(project_file_name)
    flag = False
    # Empty lists for our files
    frameworks = []
    sourcefiles = []
    resourceFiles = []
    otherfiles = []
    # Our matching parameters
    srcfiletypes = [".c", ".cpp", ".m", ".mm"]
    frameworktypes = [".framework"]
    resourceTypes = [".vsh", ".strings", ".fsh", ".storyboard", ".png"]
    for line in project_file:
        # Check for a PBXBuildFile, which is our frameworks, resource files, and source files
        if line.find("isa = PBXBuildFile;") > -1:
            fname = getFileNameFromPBXstring(line)
            if findOneFromListInFileName(fname, srcfiletypes):
                sourcefiles.append(fname)
            elif findOneFromListInFileName(fname, frameworktypes):
                frameworks.append(fname)
            elif findOneFromListInFileName(fname, resourceTypes):
                resourceFiles.append(fname)
            else:
                # Couldn't determine the file type, so store it in this filter here
                otherfiles.append(fname)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        deconstructXcodeProject(sys.argv[1])
    else:
        print "ERROR: Must pass Xcode Project as a command line parameter"